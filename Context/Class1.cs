﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using WebApplication4.Models;





namespace Context
{
    public class Class1 : DbContext
    {

        //add-migration Facturas -outputdir  Migrations/2019
        //Remove-Migration
        public Class1()
        {

        }

        public Class1(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
             modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<Contacts>(entity =>
            {
                entity.HasKey(e => e.ContactId)
                    .HasName("PK_Contact");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(50);
            });
            */

            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10034");

            modelBuilder.Entity<Notas>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Creacion).HasMaxLength(50);
                entity.Property(e => e.Modificacion).HasMaxLength(50);
                entity.Property(e => e.Mensaje).HasMaxLength(6000);
            });

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information 
                //in your connection string, you should move it out of source code. 
                //See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance 
                //on storing connection strings.

                optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["NotasConectionString"].ConnectionString);
            }


            
        }



        public DbSet<Notas> Notas { set; get; }
    }
}
