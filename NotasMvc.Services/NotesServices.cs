﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotasMvc.Context;
using NotasMvc.Entities;

namespace NotasMvc.Services
{
    public class NotesServices
    {
        public Note First(Expression<Func<Note, Boolean>> where = null)
        {
            if (where == null)
                where = x => x.Id > 0;

            Note item;

            using (var context = new NotasDbContext())
            {
                item = context.Notes.FirstOrDefault(where);
            }

            return item;
        }


        public Boolean Save(Note item)
        {
            var ret = false;
            
            try
            {
                using (var context = new NotasDbContext())
                {
                    if (item.Id == 0)
                    {
                        item.Creation = DateTime.Now;
                        item.LastModification = DateTime.Now;
                        context.Notes.Add(item);
                    }
                    else
                    {
                        item.LastModification = DateTime.Now;
                        context.Notes.Attach(item);
                        context.Entry(item).State = EntityState.Modified;
                    }
                    
                    context.SaveChanges();
                }

                ret = true;
            }
            catch (Exception e)
            {
            }

            return ret;
        }

        public List<Note> GetCollection(Expression<Func<Note, Boolean>> where = null)
        {
            if (where == null)
                where = x => x.Id > 0;

            List<Note> items;

            using (var context = new NotasDbContext())
            {
                items = context.Notes.Where(where).ToList();
            }

            return items;
        }

        public Boolean Delete(int id)
        {
            var ret = true;

            try
            {
                using (var context = new NotasDbContext())
                {
                    var item = context.Notes.FirstOrDefault(x => x.Id == id);
                    if (item == null)
                        return ret;

                    context.Entry(item).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                ret = false;
            }

            return ret;
        }
    }
}
