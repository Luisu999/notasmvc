﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NotasMvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Notes", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Find",
                url: "{controller}/{action}/{id}&{title}&{content}",
                defaults: new { controller = "Notes", action = "Index" }
            );
        }
    }
}
