﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NotasMvc.Entities;
using NotasMvc.Services;


namespace NotasMvc.Controllers
{
    public class NotesController : Controller
    {
        private NotesServices _services = new NotesServices();

        // GET: Notes
        public ActionResult Index(string id, string title, string content)
        {
            List<Note> lista;
            if (String.IsNullOrWhiteSpace(id) && String.IsNullOrWhiteSpace(title) && String.IsNullOrWhiteSpace(content))
                lista = _services.GetCollection();
            else
            {
                id = (id ?? String.Empty).Trim();
                title = (title ?? String.Empty).Trim();
                content = (content ?? String.Empty).Trim();

                Int32.TryParse(id, out Int32 nId);
                lista = _services.GetCollection(x => (nId == 0 || x.Id == nId)
                                                     && (title.Length == 0 || x.Title.Contains(title))
                                                     && (content.Length == 0 || x.Content.Contains(content)));
            }

            return View(lista.OrderByDescending(x => x.IsAnchored).ThenBy(x => x.Id));
        }

        #region Create

        // GET: Notes/Create
        public ActionResult Create()
        {
            return View(new Note());
        }

        // POST: Notes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var item = new Note
                                {
                                    Title = collection["Title"],
                                    Content = Decript(collection["contentEditorHidden"])
                                };

                if (_services.Save(item))
                    return RedirectToAction("Index");

                return View();
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        #endregion

        #region Edit

        // GET: Notes/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_services.First(x => x.Id == id));
        }

        // POST: Notes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var item = _services.First(x => x.Id == id) ?? new Note();

            try
            {
                item.Title = collection["Title"];
                item.Content = Decript(collection["contentEditorHidden"]);

                if (_services.Save(item))
                    return RedirectToAction("Index");

                return View();
            }
            catch
            {
                return View();
            }
        }

        #endregion 


        // GET: Notes/Details/5
        public ActionResult Details(int id)
        {
            return View(_services.First(x => x.Id == id));
        }

        public ActionResult Delete(int id)
        {
            _services.Delete(id);
            return RedirectToAction("Index");
        }

        #region Find

        // GET: Notes/Create
        public ActionResult Find()
        {
            return View();
        }

        // POST: Notes/Create
        [HttpPost]
        public ActionResult Find(FormCollection collection)
        {
            var id = collection["findId"];
            var title = collection["findTitle"];
            var content = collection["findContent"];

            return RedirectToAction("Index", new { id, title, content });
        }

        #endregion


        public ActionResult Anchored(int id)
        {
            var item = _services.First(x => x.Id == id);
            item.IsAnchored = !item.IsAnchored;
            _services.Save(item);

            return RedirectToAction("Index");
        }






        public String Encript(string text)
        {
            return text.Replace("<", "_lt_").Replace(">", "_gt_");
        }

        public String Decript(string text)
        {
            return text.Replace("_lt_", "<").Replace("_gt_", ">").Replace("\r\n", "<br/>");
        }



    }
}
