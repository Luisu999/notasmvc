﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotasMvc.Entities;

namespace NotasMvc.Context
{
    public class NotasDbContext : DbContext
    {

        //add-migration Facturas -outputdir  Migrations/2019
        //Remove-Migration
        public NotasDbContext()
        {

        }

        public NotasDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
             modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<Contacts>(entity =>
            {
                entity.HasKey(e => e.ContactId)
                    ;

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(50);
            });
            */

            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10034");

            modelBuilder.Entity<Note>(entity =>
                                        {
                                            entity.HasKey(e => e.Id).HasName("PK_Note");
                                            entity.Property(e => e.Creation);
                                            entity.Property(e => e.LastModification);
                                            entity.Property(e => e.Title).HasColumnType("varchar(100)");
                                            entity.Property(e => e.Content).HasColumnType("varchar(MAX)").HasMaxLength(6000);
                                            entity.Property(e => e.IsAnchored);
                                        }
                                    );

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information 
                //in your connection string, you should move it out of source code. 
                //See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance 
                //on storing connection strings.

                optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["NotesConectionString"].ConnectionString);
            }
        }



        public DbSet<Note> Notes { set; get; }
    }
}
