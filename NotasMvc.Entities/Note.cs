﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NotasMvc.Entities
{
    public class Note
    {
        public Int32 Id { set; get; }

        [Display(Name = "Creado")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime Creation { set; get; }

        [Display(Name = "Modificado")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime LastModification { set; get; }

        [Required(ErrorMessage = "El campo Titulo es obligatorio")]
        [Display(Name = "Titulo")]
        public String Title { set; get; }
        
        [Display(Name = "Nota")]
        public String Content { set; get; }
        public Boolean IsAnchored { set; get; }
    }
}
